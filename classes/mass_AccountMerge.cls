//Implementing statefull database class
global class mass_AccountMerge implements Database.Batchable<sObject>,database.stateful{
 
  //List to maintain all the fields that wll be copied to surviovr account
  list<string>sobjectFieldList= new list<string>();

    global Database.querylocator start(Database.BatchableContext BC){
    
      string query='Select id, Survivor_Account__c, victim_Account__c';
      string status='unProcessed';
      
      /** Creating Dynamic Query, to query all the updateable fields of account using lookup relationship **/

      SObjectType sobjType = Schema.getGlobalDescribe().get('Account');
      Map<String,Schema.SObjectField> sobjfields = sobjType.getDescribe().fields.getMap();
      for(string fields: sobjfields.keyset()){
        Schema.DescribeFieldResult dfr=sobjfields.get(fields).getdescribe();
        if(dfr.isupdateable()){
            sobjectFieldList.add(dfr.getName());
            query+=', Survivor_Account__r.'+dfr.getName();
            query+=', victim_Account__r.'+dfr.getName();
          }
      }

      System.debug(query+' from Account_Merge__c where processed__c=:status');
      return Database.getQueryLocator(query+' from Account_Merge__c where processed__c=:status');

    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
      
      list<Account_Merge__c> li= new list<Account_Merge__c>();
      System.debug('entered in the Batch');
      for(sObject record:scope) {
        Account_Merge__c accMerge=(Account_Merge__c)record;
        Account accSurvivor=new Account(Id=accMerge.Survivor_Account__c);
        Account accVictim=new Account(Id=accMerge.Victim_Account__c);
        
        //looping through all the fields and copying values if survivor fields are account 
        for(string sObJField:sobjectFieldList){
          system.debug('Survivor Accounts ' + sObJField + accMerge.getSobject('Survivor_Account__r').get(sObJField));
          if(accMerge.getsObject('Survivor_Account__r').get(sObJField)==null){
            accSurvivor.put(sObJField,accMerge.getsObject('victim_Account__r').get(sObJField)) ;
          }
        }
        system.debug(accSurvivor);
        system.debug(accVictim);
        try {
           Database.merge(accSurvivor,accVictim);
           accMerge.Processed__c = 'Processed';
           accMerge.Error_Details__c='';
           accMerge.Victim_Account__c=null;
        } catch(exception e) {
            accMerge.Processed__c = 'Failed';
            accMerge.Error_Details__c=e.getmessage();
        }
        li.add(accMerge);
      }
      update li;
        }


   // Email that will be trigger when the batch processing has been finished
   global void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {'abhilash.udit@yahoo.com'});
        mail.setReplyTo('noreply@alexav.com');
        mail.setSenderDisplayName('Batch owner update process 101');
        mail.setSubject('Batch Process Completed');
        mail.setPlainTextBody('This bacth process have worked find and completed ');

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   
   }
}