Public with sharing class UpdateRegionInfo{


 public static void UpdateRegionMethod(list<account> accToUpdate){
   Global_Regions__mdt[] GlobalRegions=[Select DeveloperName,Sub_Regions__c from Global_Regions__mdt];
   Sub_Regions__mdt[] SubRegions=[Select DeveloperName,Countries__c from Sub_Regions__mdt];
   Map<string,set<String>> GlobalRegionsMap= new Map<string,set<String>>();
   for(Global_Regions__mdt gl:GlobalRegions){
      GlobalRegionsMap.put(gl.DeveloperName, new set<string>(gl.Sub_Regions__c.split(',')));
   }
   Map<string,set<String>> SubRegionsMap= new Map<string,set<String>>();
   for(Sub_Regions__mdt sub:SubRegions){
      SubRegionsMap.put(sub.DeveloperName, new set<string>(sub.Countries__c.split(',')));
   }
   for(account acc:accToUpdate){
     for(string subReg:SubRegionsMap.keyset()){
        system.debug(SubRegionsMap.get(subReg).contains(acc.SDO__C));
        if(SubRegionsMap.get(subReg).contains(acc.SDO__C)){
        
          for(string GlobReg:GlobalRegionsMap.keyset()){
             if(GlobalRegionsMap.get(GlobReg).contains(subReg)){
               acc.Global_Region__c=GlobReg;
               acc.Region__c=subReg;
             }
          }
        }
     }
   }
 
 }


}