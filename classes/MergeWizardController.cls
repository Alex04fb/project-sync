public with sharing class MergeWizardController{
   public string infoMessages {get;set;}
   public boolean HideExecute{get;set;}
   public Document document {
    get {
      if (document == null)
        document = new Document();
      return document;
    }
    set;
  }
  private boolean success=false;
  public boolean getsuccess(){
   if(success==null)
        success=false;
    return success;
  }
  private boolean error=false;
  public boolean geterror(){
   if(error==null)
        error=false;
    return error;
  }
  private boolean upload=true;
  public boolean getupload(){
   if(upload==null)
        upload=true;
    return upload;
  }
  public string[] csvFileRows;
  public string fileBodyAsString;
  private map<string,string> AccountIdAndOwnerIdpair;
  public integer sizeofFIle{get;set;}
  
  public pagereference readImportedFile(){
  
      try{
           fileBodyAsString=document.body.tostring();
           csvFileRows=fileBodyAsString.split('\n');
           AccountIdAndOwnerIdpair=new map<string,string>();
           sizeoffile=csvFileRows.size();
           system.debug('line 41 = '+sizeoffile);
           for(integer i=1;i<csvFileRows.size();i++){
             string[] csvEachcolumnData=csvFileRows[i].split(',');
             system.debug(Id.valueof(csvEachcolumnData[1].replaceAll( '\\s+', '')));
             AccountIdAndOwnerIdpair.put(csvEachcolumnData[0], Id.valueof(csvEachcolumnData[1].replaceAll( '\\s+', '')));
             success=HideExecute=true;
             infoMessages='Your file has been uploaded successfully. There are total '+sizeofFIle+' records. Press submit to process the records.';
             upload=error=false;
           
           }
      
      }catch(Exception e){
      system.debug(e);
        apexpages.addmessages(e);
        apexpages.addmessage(new apexpages.message(apexpages.severity.error,'An Error Occured while reading Csv File'));
        error=true;
        system.debug(success);
        upload=success=false;
      }
      
     return null;
  
  }
    public  void startApexJob(map<id,id> csvDataMap){
    /*accountlist=csvDataMap.keyset();
    if(!Accountlist.isempty()){
        system.debug('We have read the list here is the size = '+accountlist.size())
        CustomerProessingBatch objClass = new CustomerProessingBatch();
    Database.executeBatch (objClass,200);*/
  }
  public pagereference executeBatch(){
   HideExecute=false;
   if(!error && success && !AccountIdAndOwnerIdpair.keyset().isempty()){
       CustomerProessingBatch objClass = new CustomerProessingBatch();
       objclass.accountUpdateMap=AccountIdAndOwnerIdpair;
       Database.executeBatch(objclass,20);  
       infoMessages='Request has been submitted.You will receive and email on completion.';
      }
   return null;
  
  }
}