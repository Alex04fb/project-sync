/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MergeBatch implements Database.Batchable<String>, Database.Stateful {
    global MergeBatch(Map<String,List<String>> map_for_unique_duplicate, Map<String,List<String>> map_for_unique_duplicate1, Boolean mergeall, String conditionValueOfPrm, String selectConditionField, String selectCondition, Boolean IsPersonAccount, String selectobject, Map<Id,List<String>> mapForPrimaryRecord, List<String> lstForUniqueRecord, String searchConfigId) {

    }
    global void execute(Database.BatchableContext BC, List<String> lstUniqueRecord) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global System.Iterable start(Database.BatchableContext info) {
        return null;
    }
}
