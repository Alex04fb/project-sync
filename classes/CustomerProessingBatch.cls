//Batch Job for Processing the Records
global class CustomerProessingBatch implements Database.Batchable<sobject>{
  global String [] email = new String[] {'abhilash.mishra@aon.com'};//Add here your email address here
  global Map<string,string> accountUpdateMap;
  
  global list<account> start (Database.BatchableContext BC) {
    list<account> AccountList= new list<account>();
    for(string s:accountUpdateMap.keyset()){
    system.debug(ID.valueof(accountUpdateMap.get(s)));
      AccountList.add(new account(id=s,ownerid=ID.valueof(accountUpdateMap.get(s))));
    }
    return AccountList;
   }
  //Execute method
  global void execute (Database.BatchableContext BC, List<account> scope) {
    /*List<account> customerList = new List<account>();
    List<account> updtaedCustomerList = new List<account>();//List to hold updated customer
    if(!scope.isempty()){
    for (sObject objScope: scope) { 
        account newObjScope = (account)objScope ;//type casting from generic sOject to account
        newObjScope.Description = 'updated this account'+newObjScope.id;
        newObjScope.accountnumber = newObjScope.id;
        updtaedCustomerList.add(newObjScope);//Add records to the List
        System.debug('Value of UpdatedCustomerList '+updtaedCustomerList);
    } */
        if (scope!= null && scope.size()>0) {//Check if List is empty or not
            Database.update(scope); System.debug('List Size '+scope.size());//Update the Records
        }
  
  }
  

  //Finish Method
  global void finish(Database.BatchableContext BC){
     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

  //Below code will fetch the job Id
  AsyncApexJob a = [Select a.TotalJobItems, a.Status, a.NumberOfErrors, a.JobType, a.JobItemsProcessed, a.ExtendedStatus, a.CreatedById, a.CompletedDate From AsyncApexJob a WHERE id = :BC.getJobId()];//get the job Id
  System.debug('$$$ Jobid is'+BC.getJobId());

  //below code will send an email to User about the status
  mail.setToAddresses(email);
  mail.setReplyTo('test@test.com');//Add here your email address
  mail.setSenderDisplayName('Apex Batch Processing Module');
  mail.setSubject('Batch Processing '+a.Status);
  mail.setPlainTextBody('The Batch Apex job processed  '+a.TotalJobItems+'batches with  '+a.NumberOfErrors+'failures'+'Job Item processed are'+a.JobItemsProcessed);
  Messaging.sendEmail(new Messaging.Singleemailmessage [] {mail});
  
  }
}