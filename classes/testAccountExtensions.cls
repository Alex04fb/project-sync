public with sharing class testAccountExtensions{
   
   Private Final Account acc;
  
   public testAccountExtensions(apexpages.standardController stdCtrl){
     acc= (account)stdCtrl.getRecord();
   }

    public pagereference copyBillingAddressToShippingAddress(){
        acc.shippingstreet=acc.billingstreet;
        acc.shippingcity=acc.billingcity;
        acc.shippingstate=acc.billingstate;
        acc.shippingpostalcode=acc.billingpostalcode;
        acc.shippingCountry=acc.billingCountry;
        
        return null;
    
    }
}