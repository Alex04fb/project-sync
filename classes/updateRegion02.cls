public with sharing class updateRegion02{

   public static void UpdateRegionMethod02(list<account> accToUpdate){
   Countries__c[] countries=[select Country__c, SubRegionsRegions__c from Countries__c];
   map<string, list<string>> RegionMap= new map<string, list<string>>();
   for(Countries__c c:countries){
      RegionMap.put(c.Country__c,c.SubRegionsRegions__c.split(','));
   }
   
   for(Account acc:accToUpdate){
   if(RegionMap.containsKey(acc.SDO__c)){
       acc.Global_Region__c=RegionMap.get(acc.SDO__C)[1];
      acc.Region__c=RegionMap.get(acc.SDO__C)[0];
   }
   
   }
   
   }
}