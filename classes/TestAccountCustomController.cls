public with sharing Class TestAccountCustomController{

    private Account acc;
    public boolean flag{get;set;}
    public string str {get;set;}
    public void setAcc(account value){
        acc=value;
    }
    
    public Account getacc(){
        if(acc==null) acc=new account();
        return acc; 
         
    }
    public list<account> getaccountlist(){
     list<account> li;
     try{
       li=[select id,name, industry from account limit 10];
     }catch(exception e){
       
     }
     return li;
    }
    
    
    public pagereference SaveAccountDetails(){
        acc.billingcountry=str;
        try{
            insert acc; 
        }catch(exception e){
            apexpages.addmessages(e);
            return null;
        }
       return page.StandardCtrlpage;
    
    }
    
    public pagereference Cancel(){
     return new apexpages.standardController(acc).cancel();
    
    
    }
    
    public pagereference copyBillingAddressToShippingAddress(){
        acc.shippingstreet=acc.billingstreet;
        acc.shippingcity=acc.billingcity;
        acc.shippingstate=acc.billingstate;
        acc.shippingpostalcode=acc.billingpostalcode;
        acc.shippingCountry=acc.billingCountry;
        
        return null;
    
    }
    
}