public with sharing class OwnerUpdateWizardController{
    
   public string  objectName {get;set;} // Declare the object Name , whose owner is to be updated  
   public string  attachmentid {get;set;} 
   public string csvAsString{get;set;}
   public boolean success {get;set;}
   public String[] csvFileLines{get;set;}
   public list<account> accountlist{
       get{
           if (accountlist == null)
               accountlist = new list<account>();
            return accountlist;
       }
       set;
   }
   // attachment object to store CSV as a case Attachment
   public Attachment attachment {
      get {
          if (attachment == null)
            attachment = new Attachment();
           return attachment;
        }
      set;
   }
 
   
   // This will return the list of the  objects available for owner update. 
   public list<selectoption> getObjectoptions(){
       list<selectoption> li = new list<selectoption>();
       li.add(new selectoption('Account','Account'));
       li.add(new selectoption('Contact','Contact'));
       li.add(new selectoption('opportunity','Leads And Opportunities'));
       
       return li;
   }
   //This function will log a case and update a csv as an attachement.
   
   public pagereference UploadAttachment(){
     case ca= new case();
     ca.status='New';
     ca.Priority='Low';
     Ca.origin='Email';
     if(attachment.body!=null){
        string parentid=null; 
         try{
           insert ca;
            parentid=ca.id;
          }catch(Exception e){
              apexpages.addmessages(e);
              return null;
          }
         if(parentid!=null){
             
             attachment.OwnerId = UserInfo.getUserId();
             attachment.ParentId = parentid;// the record the file is attached to
             attachment.IsPrivate = true;
             attachment.ContentType = 'text/plain; charset=UTF-8';   
             try{
                insert attachment; 
                attachmentid=attachment.id ;
                success=true;
                apexpages.addmessage(new apexpages.message(apexpages.severity.info,' File uploaded successfully'));
             }catch(Exception e){
                 apexpages.addmessages(e);
                 
             }finally{
                  attachment = new Attachment(); 
                  attachment.body=null;
             }
          }
     }
     else{
         apexpages.addmessage(new apexpages.message(apexpages.severity.error,' File is Empty or corrupt. Please upload a valid file'));
     }
    return null;
   }
   
   //This function will fetch the attachement from case and Process the list;
   
   public pagereference Updateowners(){
      attachment attch=null;
      try{
          attch=[select id, body from attachment where id=:attachmentid LIMIT 1];
      }catch(exception e){
        apexpages.addmessages(e);
      }
      if(attch==null || attch.body==null){
         apexpages.addmessage(new apexpages.message(apexpages.severity.error,'can not fetch the CSV File '));
        return null;
      }else{
       apexpages.addmessage(new apexpages.message(apexpages.severity.info,'attachment has been read  '+attch.id));
      }
      try{
           csvAsString = attch.body.toString();
           csvFileLines = csvAsString.split('\n'); 
            
           for(Integer i=1;i<csvFileLines.size();i++){
               Account accObj = new Account() ;
               string[] csvRecordData = csvFileLines[i].split(',');
               accObj.id= csvRecordData[0] ;             
              
               accObj.billingcountry = csvRecordData[1];
                                                                                       
               accountlist.add(accObj);   
           }
        //insert acclist;
        }catch(exception e){
        apexpages.addmessages(e);
         apexpages.addmessage(new apexpages.message(apexpages.severity.error,'An error occured  '));
        
        }
      return null;
   
   }
   

}