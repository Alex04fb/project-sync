public class AccountQueryAction {
  @InvocableMethod
  public static void getAccountNames(List<ID> ids) {
    opportunity opp =new opportunity();
    opp.name='Process builder opp';
    opp.accountid=ids[0];
    opp.description=JSON.Serialize(ids);
    opp.stageName='Prospecting';
    opp.closedate=system.today().addDays(14);
    insert opp;
  }
}