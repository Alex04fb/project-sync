public class AccountSelectClassController{
 
    //Our collection of the class/wrapper objects wrapAccount
    public List<wrapAccount> wrapAccountList {get; set;}
    public List<Account> selectedAccounts{get;set;}
 
    public AccountSelectClassController(){
      selectedAccounts=[select Id, Name,BillingState, Website, Phone from Account limit 10];
        integer ints=1;
        if(wrapAccountList == null) {
            wrapAccountList = new List<wrapAccount>();
            for(Account a: [select Id, Name,BillingState, Website, Phone from Account limit 10]) {
                // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                wrapAccountList.add(new wrapAccount(a,ints++));
            }
        }
    }
 
    public void processSelected() {
    
    }
 
    // This is our wrapper/container class. In this example a wrapper class contains both the standard salesforce object Account and a Boolean value
    public class wrapAccount {
        public Account acc {get; set;}
        public integer selected {get; set;}
         public boolean selected2 {get; set;}
        
 
        public wrapAccount(Account a, integer i) {
            acc = a;
            selected =i ;
            selected2 =true;
        }
    }
    
   
}