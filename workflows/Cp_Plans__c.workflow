<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Unique_Key</fullName>
        <field>CP_Plan_Unique_Id__c</field>
        <formula>Account__r.Id+&apos;-&apos;+TEXT(Year__c)+&apos;-&apos;+TEXT(Plan_Type__c)</formula>
        <name>Update Unique Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Unique Key</fullName>
        <actions>
            <name>Update_Unique_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Cp_Plans__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
