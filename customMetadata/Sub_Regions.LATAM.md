<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>LATAM</label>
    <protected>false</protected>
    <values>
        <field>Countries__c</field>
        <value xsi:type="xsd:string">ARGENTINA,BAHAMAS,BELIZE,BOLIVIA,BRAZIL,BRITISH VIRGIN ISLANDS,CHILE,COLOMBIA,COSTA RICA,DOMINICAN REPUBLIC,ECUADOR,EL SALVADOR,GUATEMALA,GUYANA,HONDURAS,MEXICO,NICARAGUA,PANAMA,PARAGUAY,PERU,SURINAME,URUGUAY,VENEZUELA</value>
    </values>
</CustomMetadata>
